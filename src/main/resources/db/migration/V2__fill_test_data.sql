-- Sites
INSERT INTO sites (id, name, latitude, longitude)
VALUES (
           'd1e45a35-d197-4144-b08a-9003041456df',
           'Red Square',
           55.754093,
           37.620407
       );

INSERT INTO sites (id, name, latitude, longitude)
VALUES (
           'c7defc7f-3dcd-4bea-88b3-c0b2f81dcf23',
           'Palace Square',
           59.939832,
           30.31456
       );

INSERT INTO sites (id, name, latitude, longitude)
VALUES (
           'd9aea36f-40e0-45c0-8ea8-6d2dfaaac46b',
           'Pokrovskaya Street',
           56.322222,
           44.000556
       );

-- Spots
INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
        '0344b9e0-8d14-40da-b054-b0fb3ff9a5d2',
        'A1',
        false,
        'street',
        'd1e45a35-d197-4144-b08a-9003041456df'
);

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           '703fbbb0-df62-4b74-bda4-e0b40aeed91b',
           'A2',
           false,
           'street',
           'd1e45a35-d197-4144-b08a-9003041456df'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           '5c5027be-1887-443b-af31-9f0e235140ee',
           'A3',
           false,
           'street',
           'd1e45a35-d197-4144-b08a-9003041456df'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           '4bc10755-5b97-42ca-9d53-d03bc5dd7948',
           'B1',
           false,
           'street',
           'd1e45a35-d197-4144-b08a-9003041456df'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           'c50f083e-a72a-4f38-b716-5bda2d2616fa',
           'B2',
           false,
           'street',
           'd1e45a35-d197-4144-b08a-9003041456df'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           '0765f1e9-7c5d-4216-8637-e71434380f60',
           'B3',
           false,
           'street',
           'd1e45a35-d197-4144-b08a-9003041456df'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           'aed94a71-bd1d-48f1-921a-e1e396fc0c0f',
           'A1',
           false,
           'underground',
           'c7defc7f-3dcd-4bea-88b3-c0b2f81dcf23'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           'f355181f-014d-4963-89ab-d3259c286cf6',
           'A2',
           false,
           'underground',
           'c7defc7f-3dcd-4bea-88b3-c0b2f81dcf23'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           'c482c123-ab78-496c-9ffc-781e2442c146',
           'A3',
           false,
           'underground',
           'c7defc7f-3dcd-4bea-88b3-c0b2f81dcf23'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           '6fde967c-0b0e-467d-9fd3-69e6f295fe62',
           'B1',
           false,
           'underground',
           'c7defc7f-3dcd-4bea-88b3-c0b2f81dcf23'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           'ca325737-503e-48e0-a9eb-54cbc2b268cd',
           'B2',
           false,
           'underground',
           'c7defc7f-3dcd-4bea-88b3-c0b2f81dcf23'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           'db62c126-fc5c-4e9d-8ae2-67afa6ed812c',
           'B3',
           false,
           'underground',
           'c7defc7f-3dcd-4bea-88b3-c0b2f81dcf23'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           'f3b8e833-620a-4d1c-a528-571a1f36b597',
           'A1',
           false,
           'parking',
           'd9aea36f-40e0-45c0-8ea8-6d2dfaaac46b'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           '6bf8c1b3-c9e3-4023-84a1-2e5f432de3c1',
           'A2',
           false,
           'parking',
           'd9aea36f-40e0-45c0-8ea8-6d2dfaaac46b'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           '23def270-4d74-435e-b231-1a80030ba040',
           'A3',
           false,
           'parking',
           'd9aea36f-40e0-45c0-8ea8-6d2dfaaac46b'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           'c2349da9-9126-4fda-994f-b677bc7fafa5',
           'B1',
           false,
           'parking',
           'd9aea36f-40e0-45c0-8ea8-6d2dfaaac46b'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           '38ee7a77-8fc3-4f42-80d5-030119a0c983',
           'B2',
           false,
           'parking',
           'd9aea36f-40e0-45c0-8ea8-6d2dfaaac46b'
       );

INSERT INTO spots (id, name, booked, location, parking_site)
VALUES (
           '66fe4bd8-04f2-4071-937b-32fdbbff25ae',
           'B3',
           false,
           'parking',
           'd9aea36f-40e0-45c0-8ea8-6d2dfaaac46b'
       );

-- Staff
INSERT INTO staff (id, name, blocked)
VALUES (
            'ac6c1f10-fff3-4a50-a382-a0fd10096c8e',
            'Alexander',
            false
        );

INSERT INTO staff (id, name, blocked)
VALUES (
           '8f4bb945-7957-43ff-9bc6-eca768fe7fcb',
           'Andrey',
           false
       );

INSERT INTO staff (id, name, blocked)
VALUES (
           'fa293b6b-929d-41e5-8d05-59c19908ee54',
           'Timofei',
           false
       );

-- Vehicles
INSERT INTO vehicles (id, model, reg_plate)
VALUES (
            '3949711e-fffb-487c-b4e7-0d06e48a4c84',
            'Toyota Chaser',
            'А001АА77'
        );

INSERT INTO vehicles (id, model, reg_plate)
VALUES (
           'e88c315f-bb4b-4425-9e63-268ee79ae4e1',
           'BMW X5',
           'А002АЕ77'
       );

INSERT INTO vehicles (id, model, reg_plate)
VALUES (
           '4de9f6a7-4a49-4659-821e-fc78a534d50e',
           'Hyundai Solaris',
           'С326НУ72'
       );