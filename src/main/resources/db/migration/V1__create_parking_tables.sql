DROP TABLE IF EXISTS spots CASCADE;
DROP TABLE IF EXISTS vehicles CASCADE;
DROP TABLE IF EXISTS staff CASCADE;
DROP TABLE IF EXISTS bookings CASCADE;
DROP TABLE IF EXISTS sites CASCADE;

CREATE TABLE sites
(
    id        UUID PRIMARY KEY,
    name      VARCHAR(200) NOT NULL,
    latitude  DECIMAL      NOT NULL,
    longitude DECIMAL      NOT NULL,
    CONSTRAINT lat_long_unique UNIQUE (latitude, longitude)
);

CREATE TABLE spots
(
    id           UUID PRIMARY KEY,
    name         VARCHAR(50) NOT NULL,
    booked       BOOLEAN     NOT NULL DEFAULT FALSE,
    location     VARCHAR(50) NOT NULL,
    parking_site UUID        NOT NULL,
    FOREIGN KEY (parking_site) REFERENCES sites (id) ON DELETE CASCADE
);

CREATE TABLE vehicles
(
    id        UUID PRIMARY KEY,
    model     VARCHAR(100) NOT NULL,
    reg_plate VARCHAR(50)  NOT NULL UNIQUE
);

CREATE TABLE staff
(
    id      UUID PRIMARY KEY,
    name    VARCHAR(100) NOT NULL,
    blocked BOOLEAN      NOT NULL DEFAULT FALSE
);

CREATE TABLE bookings
(
    id          UUID NOT NULL,
    staff_id    UUID NOT NULL,
    vehicle_id  UUID NOT NULL,
    spot_id     UUID NOT NULL,
    time_start  TIMESTAMP DEFAULT current_timestamp,
    time_finish TIMESTAMP,
    FOREIGN KEY (staff_id) REFERENCES staff (id) ON DELETE CASCADE,
    FOREIGN KEY (vehicle_id) REFERENCES vehicles (id) ON DELETE CASCADE,
    FOREIGN KEY (spot_id) REFERENCES spots (id) ON DELETE CASCADE
);

create index BOOKINGS_STAFF_ID_VEHICLE_ID_INDEX
    on BOOKINGS (spot_id);