package ru.tinkoff.parking.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import ru.tinkoff.parking.validation.RegPlateConstraint;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class VehicleRequest {
    @NotEmpty
    String model;
    @RegPlateConstraint
    String regPlate;
}
