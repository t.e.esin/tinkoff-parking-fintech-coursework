package ru.tinkoff.parking.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import ru.tinkoff.parking.validation.UuidConstraint;

import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SpotRequest {
    @NotEmpty
    String name;
    @Builder.Default
    boolean booked = false;
    @NotEmpty
    String location;
    @UuidConstraint
    UUID parkingSite;
}

