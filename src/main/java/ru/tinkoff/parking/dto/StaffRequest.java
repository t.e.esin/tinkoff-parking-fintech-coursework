package ru.tinkoff.parking.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StaffRequest {
    @NotEmpty
    String name;
    @Builder.Default
    boolean blocked = false;
}
