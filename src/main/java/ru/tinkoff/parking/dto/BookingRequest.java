package ru.tinkoff.parking.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;
import ru.tinkoff.parking.validation.UuidConstraint;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookingRequest {
    @UuidConstraint
    UUID staffId;
    @UuidConstraint
    UUID vehicleId;
    @UuidConstraint
    UUID spotId;
    @Future
    DateTime timeStart;
    @NotNull
    @Range(min = 1, max = 24)
    Integer timer;
}