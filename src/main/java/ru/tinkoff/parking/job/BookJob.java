package ru.tinkoff.parking.job;

import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tinkoff.parking.service.SpotService;

import java.util.UUID;

@Slf4j
@Component
@DisallowConcurrentExecution
public class BookJob implements Job {
    @Autowired
    private SpotService spotService;

    @Override
    public void execute(JobExecutionContext context) {
        log.info("Job ** {} ** starting @ {}", context.getJobDetail().getKey().getGroup(), context.getFireTime());
        UUID id = (UUID) context.getJobDetail().getJobDataMap().get("id");
        boolean book = (boolean) context.getJobDetail().getJobDataMap().get("book");
        spotService.book(id, book);
        log.info("Job ** {} ** completed.  Next job scheduled @ {}", context.getJobDetail().getKey().getName(), context.getNextFireTime());
    }
}
