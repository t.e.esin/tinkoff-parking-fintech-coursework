package ru.tinkoff.parking.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.parking.dto.VehicleRequest;
import ru.tinkoff.parking.exception.ResponseTransfer;
import ru.tinkoff.parking.model.Vehicle;
import ru.tinkoff.parking.service.VehicleService;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/vehicles")
@AllArgsConstructor
@Slf4j
public class VehicleController {
    private final VehicleService service;

    /**
     * Add vehicle to database
     *
     * @param vehicleRequest vehicle which need to be added
     */
    @PostMapping(
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE
    )
    @Operation(
            description = "Add new vehicle to database",
            summary = "Add new vehicle"
    )
    public ResponseTransfer create(@RequestBody @Validated VehicleRequest vehicleRequest) {
        var vehicle = Vehicle.builder()
                .id(UUID.randomUUID())
                .model(vehicleRequest.getModel())
                .regPlate(vehicleRequest.getRegPlate())
                .build();
        service.insert(vehicle);
        return new ResponseTransfer("Thanks for a new vehicle!");
    }

    /**
     * Update existing vehicle with new data
     *
     * @param vehicleRequest vehicle with new data
     * @param id             path to original vehicle
     */
    @PutMapping(path = "{id}")
    @Operation(
            description = "Update existing vehicle in database",
            summary = "Update vehicle"
    )
    public ResponseTransfer update(@PathVariable UUID id, @RequestBody @Validated VehicleRequest vehicleRequest) {
        var vehicle = Vehicle.builder()
                .id(id)
                .model(vehicleRequest.getModel())
                .regPlate(vehicleRequest.getRegPlate())
                .build();
        service.update(vehicle);
        return new ResponseTransfer("Thanks for update vehicle!");
    }

    /**
     * Delete vehicle from database
     *
     * @param id vehicle id needs to be deleted
     */
    @DeleteMapping("{id}")
    @Operation(
            description = "Delete vehicle from database",
            summary = "Delete vehicle"
    )
    public ResponseTransfer delete(@PathVariable("id") UUID id) {
        service.delete(id);
        return new ResponseTransfer("Vehicle deleted successfully!");
    }

    /**
     * Gets all vehicles from db
     *
     * @return returns vehicles founded in database
     */
    @GetMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "all"
    )
    @Operation(
            description = "Get vehicles from database",
            summary = "Get vehicles"
    )
    public List<Vehicle> findAll() {
        return service.findAll();
    }

    /**
     * Gets vehicle by id from db
     *
     * @param id path to vehicle
     * @return returns vehicle by UUID
     */
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @Operation(
            description = "Get vehicle from database by id",
            summary = "Get vehicle by id"
    )
    public Vehicle findById(@RequestParam("id") UUID id) {
        return service.findById(id);
    }
}
