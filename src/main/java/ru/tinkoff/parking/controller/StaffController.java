package ru.tinkoff.parking.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.parking.dto.StaffRequest;
import ru.tinkoff.parking.exception.ResponseTransfer;
import ru.tinkoff.parking.model.Staff;
import ru.tinkoff.parking.service.StaffService;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/staff")
@AllArgsConstructor
@Slf4j
public class StaffController {

    private final StaffService service;

    /**
     * Add employee to database
     *
     * @param staffRequest employee which need to be added
     * @return
     */
    @PostMapping(
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE
    )
    @Operation(
            description = "Add new employee to database. Method checks name validity",
            summary = "Add new employee"
    )
    public ResponseTransfer create(@RequestBody @Validated StaffRequest staffRequest) {
        var staff = Staff.builder()
                .id(UUID.randomUUID())
                .name(staffRequest.getName())
                .build();
        service.insert(staff);
        return new ResponseTransfer("Thanks For a new employee!");
    }

    /**
     * Update existing employee with new data
     *
     * @param staffRequest employee with new data
     * @param id           path to original employee
     */
    @PutMapping("{id}")
    @Operation(
            description = "Update existing employee in database",
            summary = "Update employee"
    )
    public ResponseTransfer update(@PathVariable UUID id, @RequestBody @Validated StaffRequest staffRequest) {
        var staff = Staff.builder()
                .id(id)
                .name(staffRequest.getName())
                .build();
        service.update(staff);
        return new ResponseTransfer("Thanks for update employee!");
    }

    /**
     * Delete employee from database
     *
     * @param id employee id needs to be deleted
     */
    @DeleteMapping("{id}")
    @Operation(
            description = "Delete employee from database",
            summary = "Delete employee"
    )
    public ResponseTransfer delete(@PathVariable("id") UUID id) {
        service.delete(id);
        return new ResponseTransfer("Employee deleted successfully!");
    }

    /**
     * Gets all employee from db
     *
     * @return returns employee founded in database
     */
    @GetMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "/all"
    )
    @Operation(
            description = "Get employee from database",
            summary = "Get employee"
    )
    public List<Staff> findAll() {
        return service.findAll();
    }

    /**
     * Gets employee by id from db
     *
     * @param id path to employee
     * @return returns employee by UUID
     */
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @Operation(
            description = "Get employee from database by id",
            summary = "Get employee by id"
    )
    public Staff findById(@RequestParam("id") UUID id) {
        return service.findById(id);
    }

    /**
     * Block employee by id
     */
    @PutMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "/block"
    )
    @Operation(
            description = "Block employee from database",
            summary = "Block employee"
    )
    public ResponseTransfer block(@RequestParam List<UUID> id, @RequestParam("block") boolean block) {
        service.block(id, block);
        if (block) {
            return new ResponseTransfer("Blocked!");
        } else
            return new ResponseTransfer("Unblocked!");
    }

    /**
     * Statistics for all employee from db
     *
     * @return returns statistics about employee founded in database
     */
    @GetMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "/all/statistics"
    )
    @Operation(
            description = "Get employee from database",
            summary = "Get employee"
    )
    public List<HashMap<String, String>> staffStatistics() {
        return service.staffStatistics();
    }
}
