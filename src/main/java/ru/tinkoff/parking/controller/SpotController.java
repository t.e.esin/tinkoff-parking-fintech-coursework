package ru.tinkoff.parking.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.parking.dto.SpotRequest;
import ru.tinkoff.parking.exception.ResponseTransfer;
import ru.tinkoff.parking.model.Site;
import ru.tinkoff.parking.model.Spot;
import ru.tinkoff.parking.service.SpotService;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/spots")
@AllArgsConstructor
@Slf4j
public class SpotController {

    private final SpotService service;

    /**
     * Add spot to database
     *
     * @param spotRequest spot which need to be added
     */
    @PostMapping(
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE
    )
    @Operation(
            description = "Add new spot to database. Method checks name validity",
            summary = "Add new spot"
    )
    public ResponseTransfer create(@RequestBody @Validated SpotRequest spotRequest) {
        var spot = Spot.builder()
                .id(UUID.randomUUID())
                .name(spotRequest.getName())
                .location(spotRequest.getLocation())
                .parkingSite(spotRequest.getParkingSite())
                .build();
        service.insert(spot);
        return new ResponseTransfer("Thanks for a new Spot!");
    }

    /**
     * Update existing spot with new data
     *
     * @param spotRequest spot with new data
     * @param id          path to original spot
     */
    @PutMapping("/{id}")
    @Operation(
            description = "Update existing spot in database",
            summary = "Update spot"
    )
    public ResponseTransfer update(@PathVariable UUID id, @RequestBody @Validated SpotRequest spotRequest) {
        var spot = Spot.builder()
                .id(id)
                .name(spotRequest.getName())
                .location(spotRequest.getLocation())
                .build();
        service.update(spot);
        return new ResponseTransfer("Thanks for update Spot!");
    }

    /**
     * Delete spot from database
     *
     * @param id spot id needs to be deleted
     */
    @DeleteMapping("{id}")
    @Operation(
            description = "Delete spot from database",
            summary = "Delete spot"
    )
    public ResponseTransfer delete(@PathVariable("id") UUID id) {
        service.delete(id);
        return new ResponseTransfer("Spot deleted successfully!");
    }

    /**
     * Gets all spots from db
     *
     * @return returns spots founded in database
     */
    @GetMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "/all"
    )
    @Operation(
            description = "Get spots from database",
            summary = "Get spots"
    )
    public List<Spot> findAll() {
        return service.findAll();
    }

    /**
     * Gets spot by id from db
     *
     * @param id path to spot
     * @return returns spot by UUID
     */
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @Operation(
            description = "Get spot from database by id",
            summary = "Get spot by id"
    )
    public Spot findById(@RequestParam("id") UUID id) {
        return service.findById(id);
    }

    /**
     * Gets all spots by parking site id from db
     *
     * @return returns spots by parking id founded in database
     */
    @GetMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "/all/{id}"
    )
    @Operation(
            description = "Get spots from database",
            summary = "Get spots"
    )
    public List<Spot> findAllBySiteId(@PathVariable("id") UUID parkingSiteId) {
        return service.findAllBySiteId(parkingSiteId);
    }

    /**
     * Gets all free spots by parking site id from db
     *
     * @return returns free spots by parking id founded in database
     */
    @GetMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "/all/free/{id}"
    )
    @Operation(
            description = "Get free spots by site from database",
            summary = "Get free spots in site"
    )
    public List<Spot> findAllFreeBySiteId(@PathVariable("id") UUID parkingSiteId) {
        return service.findAllFreeBySiteId(parkingSiteId);
    }

    /**
     * Gets all spots grouped by parking site from db
     *
     * @return returns spots grouped by parking site founded in database
     */
    @GetMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "/all/grouped"
    )
    @Operation(
            description = "Get spots from database grouped by parking site",
            summary = "Get spots group by parking site"
    )
    public List<Site> findAllWithSiteLocation() {
        return service.findAllWithSiteLocation();
    }

    /**
     * Book spot by id
     */
    @PutMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "/book"
    )
    @Operation(
            description = "Book spot from database",
            summary = "Book spot"
    )
    public ResponseTransfer block(@RequestParam("id") UUID id, @RequestParam("book") boolean book) {
        service.book(id, book);
        if (book) {
            return new ResponseTransfer("Booked!");
        } else
            return new ResponseTransfer("Unbooked!");
    }
}