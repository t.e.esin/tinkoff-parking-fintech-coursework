package ru.tinkoff.parking.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.parking.dto.BookingRequest;
import ru.tinkoff.parking.exception.ResponseTransfer;
import ru.tinkoff.parking.model.Booking;
import ru.tinkoff.parking.service.BookingService;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/bookings")
@RequiredArgsConstructor
@Slf4j
public class BookingController {

    private final BookingService service;

    /**
     * Add booking to database
     *
     * @param bookingRequest booking which need to be added
     */
    @PostMapping(
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE
    )
    @Operation(
            description = "Add new booking to database",
            summary = "Add new booking"
    )
    public ResponseTransfer create(@RequestBody @Validated BookingRequest bookingRequest) throws Exception {
        service.insert(bookingRequest);
        return new ResponseTransfer("Thanks for a new booking!");
    }

    /**
     * Update existing booking with new data
     *
     * @param bookingRequest booking with new data
     * @param id             id of original booking
     */
    @PutMapping("/{id}")
    @Operation(
            description = "Update existing booking in database",
            summary = "Update booking"
    )
    public ResponseTransfer update(@PathVariable UUID id, @RequestBody @Validated BookingRequest bookingRequest) {
        var booking = Booking.builder()
                .id(id)
                .staffId(bookingRequest.getStaffId())
                .vehicleId(bookingRequest.getVehicleId())
                .spotId(bookingRequest.getSpotId())
                .build();
        service.update(booking);
        return new ResponseTransfer("Thanks for update booking!");
    }

    /**
     * Delete booking from database
     *
     * @param id booking id needs to be deleted
     */
    @DeleteMapping("/{id}")
    @Operation(
            description = "Delete booking from database",
            summary = "Delete booking"
    )
    public ResponseTransfer delete(@PathVariable("id") UUID id) {
        service.delete(id);
        return new ResponseTransfer("booking deleted successfully!");
    }

    /**
     * Gets all booking from db
     *
     * @return returns booking founded in database
     */
    @GetMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "/all"
    )
    @Operation(
            description = "Get booking from database",
            summary = "Get booking"
    )
    public List<Booking> findAll() {
        return service.findAll();
    }

    /**
     * Gets booking by id from db
     *
     * @param id path to booking
     * @return returns booking by UUID
     */
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @Operation(
            description = "Get booking from database by id",
            summary = "Get booking by id"
    )
    public Booking findById(@RequestParam("id") UUID id) {
        return service.findById(id);
    }

    /**
     * Gets all bookings by staff id from db
     *
     * @return returns bookings by staff id founded in database
     */
    @GetMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "/all/{id}"
    )
    @Operation(
            description = "Get bookings from database",
            summary = "Get bookings"
    )
    public List<Booking> findAllByStaffId(@PathVariable("id") UUID staffId) {
        return service.findAllByStaffId(staffId);
    }


    /**
     * Gets all info about all bookings from db
     *
     * @return returns info about bookings founded in database
     */
    @GetMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "/all/cool"
    )
    @Operation(
            description = "Get bookings with all info from database",
            summary = "Get bookings with info"
    )
    public List<HashMap<String, String>> findCoolStuff() {
        return service.findCoolStuff();
    }

    /**
     * Gets all bookings by date  from db
     *
     * @return returns bookings by date in database
     */
    @GetMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "/date"
    )
    @Operation(
            description = "Get bookings by date from database",
            summary = "Get bookings by date"
    )
    public List<HashMap<String, String>> findBookingsByDay(@RequestParam("date") String date) {
        return service.findBookingsByDay(date);
    }


    /**
     * Gets statistics about bookings
     *
     * @return returns statistics
     */
    @GetMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "/all/statistics"
    )
    @Operation(
            description = "Get booking statistics",
            summary = "Get statistics"
    )
    public HashMap<String, String> bookingStatistics() {
        return service.bookingStatistics();
    }
}
