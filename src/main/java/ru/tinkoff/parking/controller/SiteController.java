package ru.tinkoff.parking.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.parking.dto.SiteRequest;
import ru.tinkoff.parking.exception.ResponseTransfer;
import ru.tinkoff.parking.model.Site;
import ru.tinkoff.parking.service.SiteService;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/sites")
@AllArgsConstructor
@Slf4j
public class SiteController {

    private final SiteService service;

    /**
     * Add site to database
     *
     * @param siteRequest site which need to be added
     * @return
     */
    @PostMapping(
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE
    )
    @Operation(
            description = "Add new site to database. Method checks name validity",
            summary = "Add new site"
    )
    public ResponseTransfer create(@RequestBody @Validated SiteRequest siteRequest) {
        var site = Site.builder()
                .id(UUID.randomUUID())
                .name(siteRequest.getName())
                .latitude(siteRequest.getLatitude())
                .longitude(siteRequest.getLongitude())
                .build();
        service.insert(site);
        return new ResponseTransfer("Thanks For a new Site!");
    }

    /**
     * Update existing site with new data
     *
     * @param siteRequest site with new data
     * @param id          path to original site
     */
    @PutMapping(path = "{id}")
    @Operation(
            description = "Update existing site in database",
            summary = "Update site"
    )
    public ResponseTransfer update(@PathVariable UUID id, @RequestBody @Validated SiteRequest siteRequest) {
        var site = Site.builder()
                .id(id)
                .name(siteRequest.getName())
                .latitude(siteRequest.getLatitude())
                .longitude(siteRequest.getLongitude())
                .build();
        service.update(site);
        return new ResponseTransfer("Thanks for update Site!");
    }

    /**
     * Delete site from database
     *
     * @param id site id needs to be deleted
     */
    @DeleteMapping("{id}")
    @Operation(
            description = "Delete site from database",
            summary = "Delete site"
    )
    public ResponseTransfer delete(@PathVariable("id") UUID id) {
        service.delete(id);
        return new ResponseTransfer("Site deleted successfully!");
    }

    /**
     * Gets all sites from db
     *
     * @return returns sites founded in database
     */
    @GetMapping(
            produces = APPLICATION_JSON_VALUE,
            path = "all"
    )
    @Operation(
            description = "Get sites from database",
            summary = "Get sites"
    )
    public List<Site> findAll() {
        return service.findAll();
    }

    /**
     * Gets site by id from db
     *
     * @param id path to site
     * @return returns site by UUID
     */
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @Operation(
            description = "Get site from database by id",
            summary = "Get site by id"
    )
    public Site findById(@RequestParam("id") UUID id) {
        return service.findById(id);
    }


    /**
     * Gets site by id from db
     *
     * @return returns site by UUID
     */
    @GetMapping(produces = APPLICATION_JSON_VALUE, path = "count")
    @Operation(
            description = "Count free spots from database",
            summary = "Count free spots"
    )
    public List<HashMap<String, String>> countFreeParkingSpots() {
        var all = service.countFreeParkingSpots();
        log.info(all.toString());
        return all;
    }
}
