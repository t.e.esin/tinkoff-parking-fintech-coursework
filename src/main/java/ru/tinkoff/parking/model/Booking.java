package ru.tinkoff.parking.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import ru.tinkoff.parking.validation.BookingConstraint;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@BookingConstraint
public class Booking {
    UUID id;
    UUID staffId;
    UUID vehicleId;
    UUID spotId;
    Timestamp timeStart;
    Timestamp timeFinish;
}
