package ru.tinkoff.parking.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@Data
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Spot {
    private UUID id;
    private String name;
    private boolean booked;
    private String location;
    private UUID parkingSite;
}
