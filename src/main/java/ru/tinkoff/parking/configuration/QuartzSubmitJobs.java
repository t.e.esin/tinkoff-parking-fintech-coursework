package ru.tinkoff.parking.configuration;

import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import ru.tinkoff.parking.job.StatisticsJob;
import ru.tinkoff.parking.job.CountJob;

@Configuration
public class QuartzSubmitJobs {
    @Value("${configuration.schedule.booking.stats-cron}")
    private String CRON;
    @Value("${configuration.schedule.booking.poll-freq}")
    private int POLL_FREQUENCY;

    @Bean(name = "bookCount")
    public JobDetailFactoryBean jobMemberStats() {
        return QuartzConfiguration.createJobDetail(CountJob.class, "Booking counter");
    }

    @Bean(name = "bookCountTrigger")
    public SimpleTriggerFactoryBean triggerMemberStats(@Qualifier("bookCount") JobDetail jobDetail) {
        return QuartzConfiguration.createTrigger(jobDetail, POLL_FREQUENCY);
    }

    @Bean(name = "bookingStats")
    public JobDetailFactoryBean jobMemberClassStats() {
        return QuartzConfiguration.createJobDetail(StatisticsJob.class, "Booking Statistics Job");
    }

    @Bean(name = "bookingStatsTrigger")
    public CronTriggerFactoryBean triggerMemberClassStats(@Qualifier("bookingStats") JobDetail jobDetail) {
        return QuartzConfiguration.createCronTrigger(jobDetail, CRON);
    }
}
