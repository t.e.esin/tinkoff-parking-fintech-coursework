package ru.tinkoff.parking.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Value("${configuration.users.user.name}")
    private String user;
    @Value("${configuration.users.user.password}")
    private String userPassword;
    @Value("${configuration.users.admin.name}")
    private String admin;
    @Value("${configuration.users.admin.password}")
    private String adminPassword;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/swagger-ui").permitAll()
                .antMatchers("/hello").hasAnyRole("ADMIN", "USER")

                .antMatchers(HttpMethod.GET, "/bookings/all/cool", "/sites/all", "/spots/all/**")
                .hasAnyRole("USER", "ADMIN")

                .antMatchers(HttpMethod.PUT, "/bookings/")
                .hasAnyRole("USER")

                .antMatchers(HttpMethod.GET, "/bookings/**", "/sites/**", "/spots/**", "/staff/**", "/vehicles/**")
                .hasAnyRole("ADMIN")

                .antMatchers(HttpMethod.POST, "/bookings/**", "/sites/**", "/spots/**", "/staff/**", "/vehicles/**")
                .hasRole("ADMIN")

                .antMatchers(HttpMethod.PUT, "/bookings/**", "/sites/**", "/spots/**", "/staff/**", "/vehicles/**")
                .hasRole("ADMIN")

                .antMatchers(HttpMethod.DELETE, "/bookings/**", "/sites/**", "/spots/**", "/staff/**", "/vehicles/**")
                .hasRole("ADMIN")

                .and()
                .httpBasic()
                .and()
                .logout()
                .permitAll();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.inMemoryAuthentication()
                .withUser(user)
                .password("{noop}" + userPassword)
                .roles("USER");

        auth.inMemoryAuthentication()
                .withUser(admin)
                .password("{noop}" + adminPassword)
                .roles("ADMIN");
    }
}