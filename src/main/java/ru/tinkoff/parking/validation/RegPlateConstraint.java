package ru.tinkoff.parking.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ElementType.FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = RegPlateValidator.class)
public @interface RegPlateConstraint {
    String message() default "does not match the pattern";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}