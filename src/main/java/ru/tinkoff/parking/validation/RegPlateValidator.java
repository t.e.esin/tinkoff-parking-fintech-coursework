package ru.tinkoff.parking.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * RegPlate pattern for russian vehicle numbers
 * https://ru.stackoverflow.com/a/824910
 */
public class RegPlateValidator implements ConstraintValidator<RegPlateConstraint, String> {
    public static String regex = "[АВЕКМНОРСТУХ]\\d{3}(?<!000)[АВЕКМНОРСТУХ]{2}\\d{2,3}|" +
            "[АВЕКМНОРСТУХ]{2}\\d{3}(?<!000)\\d{2,3}|" +
            "d{4}(?<!0000)[АВЕКМНОРСТУХ]{2}\\d{2,3}|" +
            "[АВЕКМНОРСТУХ]{2}\\d{3}(?<!000)[АВЕКМНОРСТУХ]\\d{2,3}|" +
            "Т[АВЕКМНОРСТУХ]{2}\\d{3}(?<!000)\\d{2,3}";

    @Override
    public void initialize(RegPlateConstraint regPlate) {
    }

    @Override
    public boolean isValid(String regPlate, ConstraintValidatorContext cxt) {

        return regPlate.matches(regex);
    }
}