package ru.tinkoff.parking.validation;

import org.joda.time.DateTime;
import ru.tinkoff.parking.dto.BookingRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class BookingValidator implements ConstraintValidator<BookingConstraint, BookingRequest> {
    @Override
    public void initialize(BookingConstraint BookingRequest) {
    }

    @Override
    public boolean isValid(BookingRequest bookingRequest, ConstraintValidatorContext cxt) {
        return !bookingRequest.getTimeStart().isBefore(new DateTime().minusSeconds(1));
    }
}