package ru.tinkoff.parking.dao;

import org.apache.ibatis.annotations.*;
import ru.tinkoff.parking.model.Booking;

import java.util.*;

@Mapper
public interface BookingRepository {
    @Insert("""
            INSERT INTO BOOKINGS (ID, STAFF_ID, VEHICLE_ID, SPOT_ID, TIME_START, TIME_FINISH)
            VALUES (#{id}::uuid, #{staffId}::uuid, #{vehicleId}::uuid, #{spotId}::uuid,
            #{timeStart}, #{timeFinish})
            """)
    void insert(Booking booking);

    @Update("""
            UPDATE BOOKINGS
            SET STAFF_ID = #{staffId}::uuid, VEHICLE_ID = #{vehicleId}::uuid,
            TIME_START = #{timeStart}, TIME_FINISH = #{timeFinish}
            WHERE ID = #{id}::uuid
            """)
    void update(Booking booking);

    @Delete("""
            DELETE
            FROM BOOKINGS
            WHERE ID = #{id}::uuid
            """)
    void delete(UUID id);

    @Select("""
            Select *
            FROM BOOKINGS
            WHERE STAFF_ID = #{staffId}::uuid AND VEHICLE_ID = #{vehicleId}:uuid
            """)
    @Results(value = {
            @Result(property = "bookings", column = "bookings")
    })
    Optional<Booking> findByStaffVehicleIds(UUID id);

    @Select("""
            SELECT *
            FROM BOOKINGS
            """)
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "staffId", column = "staff_id"),
            @Result(property = "vehicleId", column = "vehicle_id"),
            @Result(property = "spotId", column = "spot_id"),
            @Result(property = "timeStart", column = "time_start"),
            @Result(property = "timeFinish", column = "time_finish")
    })
    List<Booking> findAll();

    @Select("""
            SELECT *
            FROM BOOKINGS
            WHERE STAFF_ID = #{staffId}::uuid
            ORDER BY TIME_START
            """)
    @Results(value = {
            @Result(property = "bookings", column = "bookings")
    })
    List<Booking> findAllByStaffId(UUID staffId);

    @Select("""
            SELECT *
            FROM BOOKINGS
            WHERE VEHICLE_ID = #{vehicleId}::uuid
            ORDER BY TIME_START
            """)
    @Results(value = {
            @Result(property = "bookings", column = "bookings")
    })
    List<Booking> findAllByVehicleId(UUID vehicleId);

    @Select("""
            Select *
            FROM BOOKINGS
            WHERE ID = #{id}::uuid
            """)
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "staffId", column = "staff_id"),
            @Result(property = "vehicleId", column = "vehicle_id"),
            @Result(property = "spotId", column = "spot_id"),
            @Result(property = "timeStart", column = "time_start"),
            @Result(property = "timeFinish", column = "time_finish")
    })
    Optional<Booking> findById(UUID id);

    @Select("""
            SELECT sf.name as employeeName, model, reg_plate,
            sp.name as spotName, location, st.name as siteName,
            latitude::text, longitude::text, b.time_start::text, b.time_finish::text
            FROM staff sf JOIN bookings b ON sf.id=b.staff_id
                          JOIN vehicles v ON b.vehicle_id=v.id
                          JOIN spots sp ON b.spot_id=sp.id
                          JOIN sites st ON st.id = sp.parking_site
            WHERE date_trunc('day', time_start)::text = #{date}
            ORDER BY b.time_start DESC
            """)
    List<HashMap<String, String>> findBookingsByDay(String date);

    @Select("""
            SELECT sf.name as employeeName, model, reg_plate,
            sp.name as spotName, location, st.name as siteName,
            latitude::text, longitude::text, b.time_start::text, b.time_finish::text
                    FROM bookings b JOIN staff sf ON b.staff_id = sf.id
                                    JOIN vehicles v ON b.vehicle_id=v.id
                                    JOIN spots sp ON b.spot_id=sp.id
                                    JOIN sites st ON st.id = sp.parking_site
            ORDER BY b.id
            """)
    List<HashMap<String, String>> findCoolStuff();

    @Select("""
                SELECT COUNT(b.id)::text "Количество бронирований",
                COUNT(DISTINCT b.staff_id)::text "Количество сотрудников",
                COUNT(DISTINCT b.vehicle_id)::text "Количество транспортных средств",
                COUNT(DISTINCT b.spot_id)::text "Количество парковочных мест",
                TO_char(AVG(b.time_finish-b.time_start),'HH24:MI') "Среднее время бронирования"
                FROM bookings b
            """)
    HashMap<String, String> bookingStatistics();

    @Select("""
    with e as (select (#{ts}, #{tf}) overlaps (time_start, time_finish) as ee
    from bookings where spot_id = #{id}::uuid)
    select count(e.ee) from e where e.ee is true
            """)
    int overlapseSpot(Date ts, Date tf, UUID id);

    @Select("""
    with e as (select (#{ts}, #{tf}) overlaps (time_start, time_finish) as ee
    from bookings where vehicle_id = #{id}::uuid)
    select count(e.ee) from e where e.ee is true
            """)
    int overlapseVehicle(Date ts, Date tf, UUID id);
}
