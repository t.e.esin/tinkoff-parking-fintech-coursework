package ru.tinkoff.parking.dao;

import org.apache.ibatis.annotations.*;
import ru.tinkoff.parking.model.Vehicle;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface VehicleRepository {
    @Insert("""
            INSERT INTO VEHICLES (ID, MODEL, REG_PLATE)
            VALUES (#{id}::uuid, #{model}, #{regPlate})
            """)
    void insert(Vehicle vehicle);

    @Update("""
            UPDATE VEHICLES
            SET MODEL = #{model}, REG_PLATE = #{regPlate}
            WHERE ID = #{id}::uuid
            """)
    void update(Vehicle vehicle);

    @Delete("""
            DELETE
            FROM VEHICLES
            WHERE ID = #{id}::uuid
            """)
    void delete(UUID id);

    @Select("""
            Select *
            FROM VEHICLES
            WHERE ID = #{id}::uuid
            """)
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "model", column = "model"),
            @Result(property = "regPlate", column = "reg_plate")
    })
    Optional<Vehicle> findById(UUID id);

    @Select("""
            SELECT *
            FROM VEHICLES
            """)
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "model", column = "model"),
            @Result(property = "regPlate", column = "reg_plate")
    })
    List<Vehicle> findAll();
}
