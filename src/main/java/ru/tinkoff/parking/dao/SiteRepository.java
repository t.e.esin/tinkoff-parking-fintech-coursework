package ru.tinkoff.parking.dao;

import org.apache.ibatis.annotations.*;
import ru.tinkoff.parking.model.Site;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface SiteRepository {
    @Insert("""
            INSERT INTO SITES (ID, NAME, LATITUDE, LONGITUDE)
            VALUES (#{id}::uuid, #{name}, #{latitude}, #{longitude})
            """)
    void insert(Site site);

    @Update("""
            UPDATE SITES
            SET NAME = #{name}, LATITUDE = #{latitude}, LONGITUDE = #{longitude}
            WHERE ID = #{id}::uuid
            """)
    void update(Site site);

    @Delete("""
            DELETE
            FROM SITES
            WHERE ID = #{id}::uuid
            """)
    void delete(UUID id);

    @Select("""
            Select *
            FROM SITES
            WHERE ID = #{id}::uuid
            """)
    @Results({
            @Result(property = "site", column = "site")
    })
    Optional<Site> findById(UUID id);

    @Select("""
            SELECT *
            FROM SITES
            """)
    @Results(value = {
            @Result(property = "site", column = "site")
    })
    List<Site> findAll();


    @Select("""
                SELECT st.id::text as siteId, st.NAME as siteName, COUNT(sp.id)::text as countfree FROM
                    sites st JOIN spots sp ON st.id::uuid = sp.parking_site::uuid
                WHERE sp.booked = FALSE
                group by st.id::uuid, st.name
                ORDER BY st.NAME
            """)
    List<HashMap<String, String>> countFreeParkingSpots();
}
