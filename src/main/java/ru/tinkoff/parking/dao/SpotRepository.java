package ru.tinkoff.parking.dao;

import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.RequestParam;
import ru.tinkoff.parking.model.Site;
import ru.tinkoff.parking.model.Spot;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface SpotRepository {
    @Insert("""
            INSERT INTO SPOTS (ID, NAME, LOCATION, PARKING_SITE)
            VALUES (#{id}::uuid, #{name}, #{location}, #{parkingSite}::uuid)
            """)
    void insert(Spot spot);

    @Update("""
            UPDATE SPOTS
            SET NAME = #{name}, LOCATION = #{location}, PARKING_SITE = #{parkingSite}::uuid
            WHERE ID = #{id}::uuid
            """)
    void update(Spot spot);

    @Delete("""
            DELETE
            FROM SPOTS
            WHERE ID = #{id}::uuid
            """)
    void delete(UUID id);

    @Select("""
            Select *
            FROM SPOTS
            WHERE ID = #{id}::uuid
            """)
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "booked", column = "booked"),
            @Result(property = "location", column = "location"),
            @Result(property = "parkingSite", column = "parking_site")
    })
    Optional<Spot> findById(UUID id);

    @Select("""
            SELECT *
            FROM SPOTS
            """)
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "booked", column = "booked"),
            @Result(property = "location", column = "location"),
            @Result(property = "parkingSite", column = "parking_site")
    })
    List<Spot> findAll();

    @Select("""
            SELECT *
            FROM SPOTS
            WHERE PARKING_SITE = #{parkingSite}::uuid
            ORDER BY NAME
            """)
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "booked", column = "booked"),
            @Result(property = "location", column = "location"),
            @Result(property = "parkingSite", column = "parking_site")
    })
    List<Spot> findAllBySiteId(UUID parkingSite);

    @Select("""
            SELECT * FROM
            sites st JOIN spots sp ON st.id = sp.parking_site
            WHERE sp.booked = FALSE AND st.id = #{id}::uuid
            """)
    List<Spot> findAllFreeBySiteId(@RequestParam("id") UUID id);

    @Update("""
            UPDATE SPOTS
            SET BOOKED = #{book}
            WHERE ID = #{id}::uuid
            """)
    void book(UUID id, boolean book);

    @Select("""
            SELECT st.id as stid, sp.id as spid, st.name as stname, sp.name as spname, latitude, longitude, booked, location, parking_site
            FROM sites st JOIN spots sp ON st.id = sp.parking_site
            ORDER BY st.id
            """)
    @ResultMap("SiteSpotMap")
    List<Site> findAllWithSiteLocation();

}
