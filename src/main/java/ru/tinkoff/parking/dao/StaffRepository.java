package ru.tinkoff.parking.dao;

import org.apache.ibatis.annotations.*;
import ru.tinkoff.parking.model.Staff;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface StaffRepository {
    @Insert("""
            INSERT INTO STAFF (ID, NAME)
            VALUES (#{id}::uuid, #{name})
            """)
    void insert(Staff staff);

    @Update("""
            UPDATE STAFF
            SET NAME = #{name}
            WHERE ID = #{id}::uuid
            """)
    void update(Staff staff);

    @Delete("""
            DELETE
            FROM STAFF
            WHERE ID = #{id}::uuid
            """)
    void delete(UUID id);

    @Select("""
            Select *
            FROM STAFF
            WHERE ID = #{id}::uuid
            """)
    @Results({
            @Result(property = "staff", column = "staff")
    })
    Optional<Staff> findById(UUID id);

    @Select("""
            SELECT *
            FROM STAFF
            """)
    @Results(value = {
            @Result(property = "staff", column = "staff")
    })
    List<Staff> findAll();

    @Update("<script>" +
            "UPDATE STAFF SET BLOCKED = #{block} WHERE ID IN " +
            "<foreach item='item' index='index' collection='ids'" +
            " open='(' separator=',' close=')'>" +
            " #{item}::uuid" +
            "</foreach>" +
            "</script>")
    void block(@Param("ids") List<UUID> id, boolean block);


    @Select("""
                SELECT sf.id::text as empId, sf.NAME as empName, COUNT(b.id)::text as bookCount,
                TO_char(AVG(b.time_finish-b.time_start),'HH24:MI') AS bookAvg
                FROM staff sf JOIN bookings b ON b.staff_id=sf.id
                GROUP BY sf.id, sf.NAME
                ORDER BY sf.id
            """)
    List<HashMap<String, String>> staffStatistics();
}
