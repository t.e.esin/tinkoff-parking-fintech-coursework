package ru.tinkoff.parking.exception;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

@Getter
public enum ApplicationError {

    SITE_NOT_FOUND("site not found", 404),
    SITE_ALREADY_EXISTS("site already exists", 400),
    SPOT_NOT_FOUND("spot not found", 404),
    SPOT_ALREADY_BOOKED("spot already booked", 400),
    EMPLOYEE_NOT_FOUND("employee not found", 404),
    BOOKING_NOT_FOUND("booking not found", 404),
    BOOKING_NOT_AVAILABLE("booking not available on weekend", 404),
    VEHICLE_NOT_FOUND("vehicle not found", 404),
    VEHICLE_ALREADY_IN_BOOK("vehicle already booked", 404);

    private final String message;
    private final int code;

    ApplicationError(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public ApplicationException exception(String args) {
        return new ApplicationException(this, args);
    }

    public static class ApplicationException extends RuntimeException {

        public final ApplicationExceptionCompanion companion;

        ApplicationException(ApplicationError error, String message) {
            super(error.message + " : " + message);
            this.companion = new ApplicationExceptionCompanion(error.code, error.message + " : " + message);
        }

        public static record ApplicationExceptionCompanion(@JsonIgnore int code, String message) {
        }
    }
}

