package ru.tinkoff.parking.exception;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.tinkoff.parking.exception.ApplicationError.ApplicationException;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {
    @Override
    @NotNull
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, @NotNull HttpHeaders headers,
            @NotNull HttpStatus status, @NotNull WebRequest request
    ) {

        List<String> errors = ex.getBindingResult().getFieldErrors().stream()
                .map(it -> it.getField() + " " + it.getDefaultMessage())
                .collect(Collectors.toList());

        return new ResponseEntity<>(Map.of("message", errors), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<ApplicationException.ApplicationExceptionCompanion> handleNotFoundException(ApplicationException e) {
        log.error("ApplicationException occurred.", e);
        return new ResponseEntity(Map.of("message", e.getMessage()), HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler(DuplicateKeyException.class)
    public ResponseEntity<DuplicateKeyException> handleAlreadyExistException(DuplicateKeyException e) {
        log.error("DuplicateKeyException occurred.", e);
        return new ResponseEntity(Map.of("message", e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Exception> handleWeekendBookingException(Exception e) {
        log.error("Exception occurred.", e);
        return new ResponseEntity(Map.of("message", e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<DataIntegrityViolationException> handleWeekendBookingException(DataIntegrityViolationException e) {
        log.error("DataIntegrityViolationException occurred.", e);
        return new ResponseEntity(Map.of("message", e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
    }
}