package ru.tinkoff.parking.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ResponseTransfer {
    private String text;
}
