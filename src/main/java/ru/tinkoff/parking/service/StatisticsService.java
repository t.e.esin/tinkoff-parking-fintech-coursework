package ru.tinkoff.parking.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.parking.dao.BookingRepository;

@Slf4j
@Service
@AllArgsConstructor
public class StatisticsService {
    private final BookingRepository bookingRepository;

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public void classStats() {
        log.info("=============");
        var bookStats = bookingRepository.bookingStatistics();
        bookStats.forEach((k, v) -> log.info("{} - {}", k, v));
        log.info("==========================");
    }
}
