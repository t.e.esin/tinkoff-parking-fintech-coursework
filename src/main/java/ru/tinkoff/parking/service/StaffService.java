package ru.tinkoff.parking.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tinkoff.parking.dao.StaffRepository;
import ru.tinkoff.parking.model.Staff;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static ru.tinkoff.parking.exception.ApplicationError.EMPLOYEE_NOT_FOUND;

@Service
@AllArgsConstructor
@Slf4j
public class StaffService {
    private final StaffRepository repository;
    private final String model = "employee";

    public void insert(Staff staff) {
        log.info("insert.in {} {}", model, staff.getId());
        repository.insert(staff);
        log.info("insert.out new {} {} has been added!", model, staff.getName());
    }

    public void update(Staff staff) {
        log.info("update.in {} {}", model, staff.getId());
        findById(staff.getId());
        repository.update(staff);
        log.info("update.out {} with id {} has been updated!", model, staff.getId());
    }

    public void delete(UUID id) {
        log.info("delete.in {} {}", model, id);
        findById(id);
        repository.delete(id);
        log.info("delete.out {} with id {} has been deleted", model, id);
    }

    public Staff findById(UUID id) {
        log.info("findById.in {} {}", model, id);
        Staff actual = repository.findById(id).orElse(null);
        if (nonNull(actual)) {
            log.info("selectById.nonNull {} found in database", model);
        } else {
            log.error("findById.thrown");
            throw EMPLOYEE_NOT_FOUND.exception(format("%s with id=%s doesn't exists", model, id));
        }
        log.info("findById.out");
        return actual;
    }

    public List<Staff> findAll() {
        log.info("findAll.in");
        List<Staff> all = repository.findAll();
        log.info("findAll.out");
        return all;
    }

    public void block(List<UUID> id, boolean block) {
        log.info("block.in");
        id.forEach(this::findById);
        repository.block(id, block);
        log.info("block.out add block status {} for {} successfully", block, id);
    }

    public List<HashMap<String, String>> staffStatistics() {
        log.info("staffStatistics.in");
        var all = repository.staffStatistics();
        log.info("staffStatistics.out");
        return all;
    }
}
