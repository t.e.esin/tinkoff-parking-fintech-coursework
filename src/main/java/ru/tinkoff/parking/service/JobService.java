package ru.tinkoff.parking.service;

import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

import static ru.tinkoff.parking.service.JobUtil.createJob;
import static ru.tinkoff.parking.service.JobUtil.createSingleTrigger;

@Slf4j
@Service
public class JobService {
    @Autowired
    @Lazy
    SchedulerFactoryBean schedulerFactoryBean;

    @Autowired
    private ApplicationContext context;

    public void scheduleBookStartFinish(String jobName, Class<? extends Job> jobClass, Date date, UUID id, boolean b) {
        log.info("Request received to scheduleBookStartFinish");
        String groupKey;
        if (b) {
            groupKey = "BookGroup";
        }
        else {
            groupKey = "UnBookGroup";
        }
        JobDetail jobDetail = createJob(jobClass, false, context, jobName, groupKey, id, b);
        log.info("Creating trigger for key: " + jobName + " at date: " + date);
        Trigger cronTriggerBean = createSingleTrigger(jobName, date, SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);

        try {
            Scheduler scheduler = schedulerFactoryBean.getScheduler();
            Date dt = scheduler.scheduleJob(jobDetail, cronTriggerBean);
            log.info("Job with jobKey:" + jobName + " scheduled successfully for date: " + dt);
        } catch (SchedulerException e) {
            log.error("SchedulerException while scheduling job with key: " + jobName + " message: " + e.getMessage());
            e.printStackTrace();
        }

    }
}
