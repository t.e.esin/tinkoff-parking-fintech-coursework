package ru.tinkoff.parking.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.tinkoff.parking.dao.BookingRepository;
import ru.tinkoff.parking.dto.BookingRequest;
import ru.tinkoff.parking.job.BookJob;
import ru.tinkoff.parking.model.Booking;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static ru.tinkoff.parking.exception.ApplicationError.*;

@Service
@AllArgsConstructor
@Slf4j
public class BookingService {
    private final BookingRepository repository;
    private final SpotService spotService;
    private final StaffService staffService;
    private final VehicleService vehicleService;
    private final String model = "booking";

    @Autowired
    @Lazy
    JobService jobService;

    public void insert(BookingRequest bookingRequest) throws Exception {
        log.info("insert.in {} to {} spot", model, bookingRequest.getSpotId());

        if (bookingRequest.getTimeStart() == null) {
            bookingRequest.setTimeStart(new DateTime());
        }
        var ts = bookingRequest.getTimeStart();
        var tf = ts.plusHours(bookingRequest.getTimer());

        if (!tf.dayOfMonth().equals(ts.dayOfMonth()))
            tf = ts.plusDays(1).withMillisOfDay(0);

        var timeStart = new Timestamp(ts.getMillis());
        var timeFinish = new Timestamp(tf.getMillis());
        var booking = Booking.builder()
                .id(UUID.randomUUID())
                .staffId(bookingRequest.getStaffId())
                .vehicleId(bookingRequest.getVehicleId())
                .spotId(bookingRequest.getSpotId())
                .timeStart(timeStart)
                .timeFinish(timeFinish)
                .build();

        ts = new DateTime(booking.getTimeStart(), DateTimeZone.forID("UTC"));

        if (ts.getDayOfWeek() > 5) {
            throw BOOKING_NOT_AVAILABLE.exception(format("Booking with id=%s was not saved", booking.getId().toString()));
        }

        if (overlapseSpot(ts, tf, booking.getSpotId()) > 0) {
            throw SPOT_ALREADY_BOOKED.exception(format("New booking with spotId=%s overlapse with existing", booking.getId().toString()));
        }

        if (overlapseVehicle(ts, tf, booking.getVehicleId()) > 0) {
            throw VEHICLE_ALREADY_IN_BOOK.exception(format("New booking with vehicleId=%s overlapse with existing", booking.getVehicleId().toString()));
        }

        var spot = spotService.findById(booking.getSpotId());
        var employee = staffService.findById(booking.getStaffId());
        var vehicle = vehicleService.findById(booking.getVehicleId());

        jobService.scheduleBookStartFinish(UUID.randomUUID().toString(), BookJob.class, new Date(ts.getMillis()), spot.getId(), true);
        jobService.scheduleBookStartFinish(UUID.randomUUID().toString(), BookJob.class, new Date(tf.getMillis()), spot.getId(), false);

        repository.insert(booking);

        log.info("insert.out new {} for {} has been added!  {}: {}", model,
                employee.getName(),
                employee.getName(),
                vehicle.getRegPlate());
    }

    public void update(Booking booking) {
        log.info("update.in {} to {} spot", model, booking.getSpotId());
        Booking old = findById(booking.getId());
        booking.setSpotId(old.getSpotId());

        var employee = staffService.findById(booking.getStaffId());
        var vehicle = vehicleService.findById(booking.getVehicleId());

        repository.update(booking);
        log.info("update.out {} for spot {} has been updated! {}: {}", model,
                booking.getSpotId(),
                employee.getName(),
                vehicle.getRegPlate());
    }

    public void delete(UUID id) {
        log.info("delete.in {} {}", model, id);
        var booking = findById(id);
        spotService.book(booking.getSpotId(), false);
        repository.delete(id);
        log.info("delete.out {} with id {} has been deleted. Spot {} is now free", model, id, booking.getSpotId());
    }

    public Booking findById(UUID id) {
        log.info("findById.in {} {}", model, id);
        Booking actual = repository.findById(id).orElse(null);
        if (nonNull(actual)) {
            log.info("findById.nonNull {} found in database", model);
        } else {
            log.error("findById.thrown");
            throw BOOKING_NOT_FOUND.exception(format("%s with id=%s doesn't exists", model, id));
        }
        log.info("findById.out");
        return actual;
    }

    public List<Booking> findAll() {
        log.info("findAll.in");
        List<Booking> all = repository.findAll();
        log.info("findAll.out");
        return all;
    }

    public List<Booking> findAllByStaffId(UUID staffId) {
        log.info("findAll.in");
        List<Booking> all = repository.findAllByStaffId(staffId);
        log.info("findAll.out");
        return all;
    }

    public List<Booking> findAllByVehicleId(UUID vehicleId) {
        log.info("findAll.in");
        List<Booking> all = repository.findAllByStaffId(vehicleId);
        log.info("findAll.out");
        return all;
    }

    public List<HashMap<String, String>> findCoolStuff() {
        log.info("findCool.in");
        List<HashMap<String, String>> all = repository.findCoolStuff();
        log.info("findCool.out");
        return all;
    }

    public List<HashMap<String, String>> findBookingsByDay(String date) {
        log.info("findBookingsByDay.in");
        var dateParse = date + " 00:00:00";
        log.info(dateParse);
        List<HashMap<String, String>> all = repository.findBookingsByDay(dateParse);
        log.info("findBookingsByDay.out");
        return all;
    }

    public HashMap<String, String> bookingStatistics() {
        log.info("bookingStatistics.in");
        HashMap<String, String> all = repository.bookingStatistics();
        log.info("bookingStatistics.out");
        return all;
    }

    public int overlapseSpot(DateTime ts, DateTime tf, UUID spotId) {
        log.info("overlapseSpot.in");
        Date tss = new Date(ts.getMillis());
        Date tfs = new Date(tf.getMillis());
        log.info(tss.toString() + tfs.toString());
        int ol = repository.overlapseSpot(tss, tfs, spotId);
        log.info("overlapseSpot.out");
        return ol;
    }

    public int overlapseVehicle(DateTime ts, DateTime tf, UUID vehicleId) {
        log.info("overlapseVehicle.in");
        Date tss = new Date(ts.getMillis());
        Date tfs = new Date(tf.getMillis());
        log.info(tss.toString() + tfs.toString());
        int ol = repository.overlapseVehicle(tss, tfs, vehicleId);
        log.info("overlapseVehicle.out");
        return ol;
    }
}
