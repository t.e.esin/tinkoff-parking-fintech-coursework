package ru.tinkoff.parking.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tinkoff.parking.dao.VehicleRepository;
import ru.tinkoff.parking.model.Vehicle;

import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static ru.tinkoff.parking.exception.ApplicationError.VEHICLE_NOT_FOUND;

@Service
@AllArgsConstructor
@Slf4j
public class VehicleService {
    private final VehicleRepository repository;
    private final String model = "vehicle";

    public void insert(Vehicle vehicle) {
        log.info("insert.in {} {}", model, vehicle.getId());
        repository.insert(vehicle);
        log.info("insert.out new {} {} has been added!", model, vehicle.getModel());
    }

    public void update(Vehicle vehicle) {
        log.info("update.in {} {}", model, vehicle.getId());
        findById(vehicle.getId());
        repository.update(vehicle);
        log.info("update.out {} with id {} has been updated!", model, vehicle.getId());
    }

    public void delete(UUID id) {
        log.info("delete.in {} {}", model, id);
        findById(id);
        repository.delete(id);
        log.info("delete.out {} with id {} has been deleted", model, id);
    }

    public Vehicle findById(UUID id) {
        log.info("findById.in {} {}", model, id);
        Vehicle actual = repository.findById(id).orElse(null);
        if (nonNull(actual)) {
            log.info("selectById.nonNull {} found in database", model);
        } else {
            log.error("selectById.thrown");
            throw VEHICLE_NOT_FOUND.exception(format("%s with id=%s doesn't exists", model, id));
        }
        log.info("findById.out");
        return actual;
    }

    public List<Vehicle> findAll() {
        log.info("findAll.in");
        List<Vehicle> all = repository.findAll();
        log.info("findAll.out");
        return all;
    }
}
