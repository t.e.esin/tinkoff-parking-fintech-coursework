package ru.tinkoff.parking.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tinkoff.parking.dao.SiteRepository;
import ru.tinkoff.parking.model.Site;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static ru.tinkoff.parking.exception.ApplicationError.SITE_NOT_FOUND;

@Service
@AllArgsConstructor
@Slf4j
public class SiteService {
    private final SiteRepository repository;
    private final String model = "site";

    public void insert(Site site) {
        log.info("insert.in {} {}", model, site.getId());
        repository.insert(site);
        log.info("insert.out new {} {} has been added!", model, site.getName());
    }

    public void update(Site site) {
        log.info("update.in {} {}", model, site.getId());
        findById(site.getId());
        repository.update(site);
        log.info("update.out {} with id {} has been updated!", model, site.getId());
    }

    public void delete(UUID id) {
        log.info("delete.in {} {}", model, id);
        findById(id);
        repository.delete(id);
        log.info("delete.out {} with id {} has been deleted", model, id);
    }

    public Site findById(UUID id) {
        log.info("findById.in {} {}", model, id);
        Site actual = repository.findById(id).orElse(null);
        if (nonNull(actual)) {
            log.info("selectById.nonNull {} found in database", model);
        } else {
            log.error("selectById.thrown");
            throw SITE_NOT_FOUND.exception(format("%s with id=%s doesn't exists", model, id));
        }
        log.info("findById.out");
        return actual;
    }

    public List<Site> findAll() {
        log.info("findAll.in");
        List<Site> all = repository.findAll();
        log.info("findAll.out");
        return all;
    }

    public List<HashMap<String, String>> countFreeParkingSpots() {
        log.info("countFreeParkingSpots.in");
        List<HashMap<String, String>> all = repository.countFreeParkingSpots();
        log.info(all.toString());
        log.info("countFreeParkingSpots.out");
        return all;
    }

}
