package ru.tinkoff.parking.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tinkoff.parking.dao.BookingRepository;
import ru.tinkoff.parking.model.Booking;

import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class CountService {
    private BookingRepository bookingRepository;

    public void bookCountStats() {
        List<Booking> bookings = bookingRepository.findAll();
        log.info("База содержит {} бронирований парковочных мест", bookings.size());
    }
}
