package ru.tinkoff.parking.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import ru.tinkoff.parking.dao.SpotRepository;
import ru.tinkoff.parking.model.Site;
import ru.tinkoff.parking.model.Spot;

import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static ru.tinkoff.parking.exception.ApplicationError.SPOT_NOT_FOUND;

@Service
@AllArgsConstructor
@Slf4j
public class SpotService {
    private final SpotRepository repository;
    private final SiteService siteService;

    private final String model = "spot";

    public void insert(Spot spot) {
        log.info("insert.in {} {}", model, spot.getId());
        siteService.findById(spot.getParkingSite());
        repository.insert(spot);
        log.info("insert.out new {} {} has been added!", model, spot.getName());
    }

    public void update(Spot spot) {
        log.info("update.in {} {}", model, spot.getId());
        Spot old = findById(spot.getId());
        log.info(old.toString());
        spot.setParkingSite(old.getParkingSite());
        repository.update(spot);
        log.info("update.out {} with id {} has been updated!", model, spot.getId());
    }

    public void delete(UUID id) {
        log.info("delete.in spot {}", id);
        findById(id);
        repository.delete(id);
        log.info("delete.out {} with id {} has been deleted", model, id);
    }

    public Spot findById(UUID id) {
        log.info("findById.in {} {}", model, id);
        Spot actual = repository.findById(id).orElse(null);
        if (nonNull(actual)) {
            log.info("findById.nonNull {} found in database", model);
        } else {
            log.error("findById.thrown");
            throw SPOT_NOT_FOUND.exception(format("%s with id=%s doesn't exists", model, id));
        }
        log.info("findById.out");
        return actual;
    }


    public List<Spot> findAllFreeBySiteId(@RequestParam("id") UUID parkingSite) {
        siteService.findById(parkingSite);
        return repository.findAllFreeBySiteId(parkingSite);
    }


    public List<Spot> findAll() {
        log.info("findAll.in");
        List<Spot> all = repository.findAll();
        log.info("findAll.out");
        return all;
    }

    public List<Spot> findAllBySiteId(UUID parkingSite) {
        log.info("findAll.in");
        siteService.findById(parkingSite);
        List<Spot> all = repository.findAllBySiteId(parkingSite);
        log.info("findAll.out");
        return all;
    }

    public List<Site> findAllWithSiteLocation() {
        log.info("findAllWithSiteLocation.in");
        List<Site> all = repository.findAllWithSiteLocation();
        log.info("findAllWithSiteLocation.out");
        return all;
    }

    public void book(UUID id, boolean book) {
        log.info("book.in");
        findById(id);
        repository.book(id, book);
        log.info("book.out add book status {} for {} successfully", book, id);
    }
}
