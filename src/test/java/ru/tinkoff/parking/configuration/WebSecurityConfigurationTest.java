package ru.tinkoff.parking.configuration;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tinkoff.parking.AbstractTest;
import ru.tinkoff.parking.service.SiteService;
import ru.tinkoff.parking.service.SpotService;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class WebSecurityConfigurationTest extends AbstractTest {

    private final ObjectMapper jackson = new ObjectMapper();
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private SiteService siteService;

    @Autowired
    private SpotService spotService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    void rootGet_NoUserSuccess() throws Exception {
        mockMvc.perform(get("/swagger-ui/"))
                .andExpect(status().isOk());
    }

    @Test
    void helloGet_NoUserDenied() throws Exception {
        mockMvc.perform(get("/hello"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void sitesGet_UserIncorrectPassword() throws Exception {
        mockMvc.perform(get("/sites/all")
                        .with(httpBasic(user, "wrong-password")))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void sitesGet_UserIncorrectUsername() throws Exception {
        mockMvc.perform(get("/sites/all")
                        .with(httpBasic("wrong-username", userPassword)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void sitesGet_UserCorrectPassword() throws Exception {
        mockMvc.perform(get("/sites/all")
                        .with(httpBasic(user, userPassword)))
                .andExpect(status().isOk());
    }

    @Test
    void sitesGet_UserSuccess() throws Exception {
        mockMvc.perform(get("/sites/all")
                        .with(user(user)))
                .andExpect(status().isOk());
    }

    @Test
    void sitesGet_AdminSuccess() throws Exception {
        mockMvc.perform(get("/sites/all")
                        .with(user(admin).password(adminPassword)))
                .andExpect(status().isOk());
    }

    @Test
    void sitesGet_NoUserDenied() throws Exception {
        mockMvc.perform(get("/sites/all")
                )
                .andExpect(status().isUnauthorized());
    }

    @Test
    void spotsGet_UserSuccess() throws Exception {
        mockMvc.perform(get("/spots/all")
                        .with(user(user)))
                .andExpect(status().isOk());
    }

    @Test
    void spotsGet_AdminSuccess() throws Exception {
        mockMvc.perform(get("/spots/all")
                        .with(user(admin).password(adminPassword)))
                .andExpect(status().isOk());
    }

    @Test
    void spotsGet_NoUserDenied() throws Exception {
        mockMvc.perform(get("/spots/all")
                )
                .andExpect(status().isUnauthorized());
    }

    @Test
    void sitesPost_AdminSuccess() throws Exception {
        var site = prepareValidSite();
        mockMvc.perform(post("/sites")
                        .with(user(admin).password(adminPassword).roles("ADMIN"))
                        .contentType("application/json")
                        .content(site))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void coursesPost_UserDenied() throws Exception {
        mockMvc.perform(post("/sites")
                        .with(user(user).password(userPassword)))
                .andExpect(status().isForbidden());
    }

    @Test
    void sitesPost_NoUserDenied() throws Exception {
        mockMvc.perform(post("/sites")
                )
                .andExpect(status().isUnauthorized());
    }

    @Test
    void spotsPost_AdminSuccess() throws Exception {
        var spot = prepareValidSpot();
        mockMvc.perform(post("/spots")
                        .with(user(admin).password(adminPassword).roles("ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON).content(spot))
                .andExpect(status().isOk());
    }

    @Test
    void spotsPost_UserDenied() throws Exception {
        mockMvc.perform(post("/spots")
                        .with(user(user).password(userPassword)))
                .andExpect(status().isForbidden());
    }

    @Test
    void spotsPost_NoUserDenied() throws Exception {
        mockMvc.perform(post("/spots")
                )
                .andExpect(status().isUnauthorized());
    }

    @Test
    void sitesDelete_AdminSuccess() throws Exception {
        jackson.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        var site = prepareValidSite();
        mockMvc.perform(post("/sites")
                .with(user(admin).password(adminPassword).roles("ADMIN"))
                .contentType(MediaType.APPLICATION_JSON).content(site));

        var siteId = siteService.findAll().get(0).getId();

        mockMvc.perform(delete("/sites/" + siteId)
                        .with(user(admin).password(adminPassword).roles("ADMIN"))
                        .accept("application/json"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void sitesDelete_UserDenied() throws Exception {
        jackson.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        var site = prepareValidSite();
        mockMvc.perform(post("/sites")
                .with(user(admin).password(adminPassword).roles("ADMIN"))
                .contentType(MediaType.APPLICATION_JSON).content(site));

        var siteId = siteService.findAll().get(0).getId();

        mockMvc.perform(delete("/sites/" + siteId)
                        .with(user(user).password(userPassword).roles("USER"))
                        .accept("application/json"))
                .andExpect(status().isForbidden())
                .andDo(print());
    }

    @Test
    void sitesDelete_NoUserDenied() throws Exception {
        jackson.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        var site = prepareValidSite();
        mockMvc.perform(post("/sites")
                .with(user(admin).password(adminPassword).roles("ADMIN"))
                .contentType(MediaType.APPLICATION_JSON).content(site));

        var siteId = siteService.findAll().get(0).getId();

        mockMvc.perform(delete("/sites/" + siteId)
                        .accept("application/json"))
                .andExpect(status().isUnauthorized())
                .andDo(print());
    }

    @Test
    void spotsDelete_AdminSuccess() throws Exception {
        jackson.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        var spot = prepareValidSpot();
        mockMvc.perform(post("/spots")
                .with(user(admin).password(adminPassword).roles("ADMIN"))
                .contentType(MediaType.APPLICATION_JSON).content(spot));

        var spotId = spotService.findAll().get(0).getId();

        mockMvc.perform(delete("/spots/" + spotId)
                        .with(user(admin).password(adminPassword).roles("ADMIN"))
                        .accept("application/json"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void spotsDelete_UserDenied() throws Exception {
        jackson.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        var spot = prepareValidSpot();
        mockMvc.perform(post("/spots")
                .with(user(admin).password(adminPassword).roles("ADMIN"))
                .contentType(MediaType.APPLICATION_JSON).content(spot));

        var spotId = spotService.findAll().get(0).getId();

        mockMvc.perform(delete("/spots/" + spotId)
                        .with(user(user).password(userPassword).roles("USER"))
                        .accept("application/json"))
                .andExpect(status().isForbidden())
                .andDo(print());
    }

    @Test
    void spotsDelete_NoUserDenied() throws Exception {
        jackson.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        var spot = prepareValidSpot();
        mockMvc.perform(post("/spots")
                .with(user(admin).password(adminPassword).roles("ADMIN"))
                .contentType(MediaType.APPLICATION_JSON).content(spot));

        var spotId = spotService.findAll().get(0).getId();

        mockMvc.perform(delete("/spots/" + spotId)
                        .accept("application/json"))
                .andExpect(status().isUnauthorized())
                .andDo(print());
    }

    @Test
    void sitesPut_AdminSuccess() throws Exception {
        var site = prepareValidSite();
        mockMvc.perform(post("/sites")
                .with(user(admin).password(adminPassword).roles("ADMIN"))
                .contentType(MediaType.APPLICATION_JSON).content(site));

        var sites = siteService.findAll();
        var siteId = sites.get(sites.size() - 1).getId();

        mockMvc.perform(put("/sites/" + siteId)
                        .with(user(admin).password(adminPassword).roles("ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON).content(site))
                .andExpect(status().isOk());
    }

    @Test
    void coursesPut_UserDenied() throws Exception {
        var site = prepareValidSite();
        mockMvc.perform(post("/sites")
                .with(user(admin).password(adminPassword).roles("ADMIN"))
                .contentType(MediaType.APPLICATION_JSON).content(site));

        var sites = siteService.findAll();
        var siteId = sites.get(sites.size() - 1).getId();

        mockMvc.perform(put("/sites/" + siteId)
                        .with(user(user).password(userPassword).roles("USER"))
                        .contentType(MediaType.APPLICATION_JSON).content(site))
                .andExpect(status().isForbidden());
    }

    @Test
    void sitesPut_NoUserDenied() throws Exception {
        var siteId = siteService.findAll().get(0).getId();
        mockMvc.perform(put("/sites/" + siteId)
                )
                .andExpect(status().isUnauthorized());
    }

    @Test
    void spotsPut_AdminSuccess() throws Exception {
        var spot = prepareValidSpot();
        mockMvc.perform(post("/spots")
                .with(user(admin).password(adminPassword).roles("ADMIN"))
                .contentType(MediaType.APPLICATION_JSON).content(spot));

        var spots = spotService.findAll();
        var spotId = spots.get(spots.size() - 1).getId();

        mockMvc.perform(put("/spots/" + spotId)
                        .with(user(admin).password(adminPassword).roles("ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON).content(spot))
                .andExpect(status().isOk());
    }

    @Test
    void studentsPut_UserDenied() throws Exception {
        var spot = prepareValidSpot();
        mockMvc.perform(post("/spots")
                .with(user(admin).password(adminPassword).roles("ADMIN"))
                .contentType(MediaType.APPLICATION_JSON).content(spot));

        var spots = siteService.findAll();
        var spotId = spots.get(spots.size() - 1).getId();

        mockMvc.perform(put("/spots/" + spotId)
                        .with(user(user).password(userPassword).roles("USER"))
                        .contentType(MediaType.APPLICATION_JSON).content(spot))
                .andExpect(status().isForbidden());
    }

    @Test
    void studentsPut_NoUserDenied() throws Exception {
        var spot = prepareValidSpot();
        mockMvc.perform(post("/spots")
                .with(user(admin).password(adminPassword).roles("ADMIN"))
                .contentType(MediaType.APPLICATION_JSON).content(spot));

        var spots = siteService.findAll();
        var spotId = spots.get(spots.size() - 1).getId();

        mockMvc.perform(put("/spots/" + spotId)
                        .contentType(MediaType.APPLICATION_JSON).content(spot))
                .andExpect(status().isUnauthorized());
    }

    private String prepareValidSite() {
        return String.format("{" +
                        "\"name\": \"%s\"," +
                        "\"latitude\": %s," +
                        "\"longitude\": %s" +
                        "}",
                generateRandomString(),
                generateRandomDecimal(-90.0, 90.0),
                generateRandomDecimal(-180.0, 180.0));
    }

    private String prepareValidSpot() {
        var parkingSite = siteService.findAll().get(0).getId();
        return String.format("{" +
                        "\"name\": \"%s\"," +
                        "\"location\": \"%s\"," +
                        "\"parkingSite\": \"%s\"" +
                        "}",
                generateRandomString(),
                generateRandomString(),
                parkingSite);
    }
}