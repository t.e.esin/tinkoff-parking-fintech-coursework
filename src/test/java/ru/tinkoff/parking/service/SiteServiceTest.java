package ru.tinkoff.parking.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.tinkoff.parking.AbstractTest;
import ru.tinkoff.parking.model.Site;

import java.math.BigDecimal;
import java.util.Random;
import java.util.UUID;


@SpringBootTest
@Slf4j
class SiteServiceTest extends AbstractTest {

    @Autowired
    private SiteService siteService;

    @Test
    public void insertSiteToDb_Success() {
        Site site = prepareValidSite();
        siteService.insert(site);
    }

    private Site prepareValidSite() {
        return new Site(
                UUID.randomUUID(),
                generateRandomString(),
                BigDecimal.valueOf(generateRandomDecimal(-90.0, 90.0)),
                BigDecimal.valueOf(generateRandomDecimal(-180.0, 180.0))
        );
    }
}