package ru.tinkoff.parking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mifmif.common.regex.Generex;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.tinkoff.parking.AbstractTest;
import ru.tinkoff.parking.exception.ApplicationError;
import ru.tinkoff.parking.service.VehicleService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.parking.exception.ApplicationError.VEHICLE_NOT_FOUND;

@WithMockUser(roles = "ADMIN")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class VehicleControllerTest extends AbstractTest {
    private final ObjectMapper jackson = new ObjectMapper();
    @Autowired
    VehicleService vehicleService;

    @Test
    public void testPost_Success() throws Exception {
        var vehicle = prepareValidVehicle();
        mockMvc.perform(post("/vehicles")
                        .contentType("application/json")
                        .content(vehicle))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testGet_Failure() throws Exception {
        var vehicleId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        mockMvc.perform(get("/vehicles").param("id", vehicleId)
                        .accept("application/json"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGet_Success() throws Exception {
        var vehicle = prepareValidVehicle();
        mockMvc.perform(post("/vehicles")
                .contentType("application/json")
                .content(vehicle));

        var vehicles = vehicleService.findAll();
        var vehicleId = vehicles.get(vehicles.size() - 1).getId();

        mockMvc.perform(get("/vehicles").param("id", vehicleId.toString())
                        .accept("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdate_Success() throws Exception {
        var vehicle = prepareValidVehicle();
        mockMvc.perform(post("/vehicles")
                .contentType("application/json")
                .content(vehicle));

        var vehicles = vehicleService.findAll();
        var vehicleId = vehicles.get(vehicles.size() - 1).getId();

        mockMvc.perform(put("/vehicles/" + vehicleId)
                        .contentType("application/json")
                        .content(vehicle))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testDelete_Success() throws Exception {
        var vehicle = prepareValidVehicle();
        mockMvc.perform(post("/vehicles/")
                .contentType("application/json")
                .content(vehicle));

        var vehicles = vehicleService.findAll();
        var vehicleId = vehicles.get(vehicles.size() - 1).getId();

        mockMvc.perform(delete("/vehicles/" + vehicleId)
                        .accept("application/json"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testPost_FailedInvalidBlockedField() throws Exception {
        var vehicle = prepareInvalidVehicle();
        mockMvc.perform(post("/vehicles")
                        .contentType("application/json")
                        .content(vehicle))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testUpdate_FailedNotEnoughFields() throws Exception {
        var vehicle = prepareValidVehicle();
        var vehicleNotEnough = prepareNotEnoughVehicle();
        mockMvc.perform(post("/vehicles")
                .contentType("application/json")
                .content(vehicle));

        var vehicles = vehicleService.findAll();
        var vehicleId = vehicles.get(vehicles.size() - 1).getId();

        mockMvc.perform(put("/vehicles/" + vehicleId)
                        .contentType("application/json")
                        .content(vehicleNotEnough))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testUpdate_FailedBlankFields() throws Exception {
        var vehicle = prepareValidVehicle();
        var vehicleBlankFields = prepareBlankFieldsVehicle();
        mockMvc.perform(post("/vehicles/")
                .contentType("application/json")
                .content(vehicle));

        var vehicles = vehicleService.findAll();
        var vehicleId = vehicles.get(vehicles.size() - 1).getId();

        mockMvc.perform(put("/vehicles/" + vehicleId)
                        .contentType("application/json")
                        .content(vehicleBlankFields))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testGet_NotFound() throws Exception {
        var vehicleId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        var notFoundException = jackson.writeValueAsString(prepareVehicleNotFoundExceptionCompanion(vehicleId));
        mockMvc.perform(get("/vehicles/").param("id", vehicleId)
                        .accept("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(notFoundException));
    }

    @Test
    public void testUpdate_NotFound() throws Exception {
        var vehicle = prepareValidVehicle();
        var vehicleId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        mockMvc.perform(put("/vehicles/" + vehicleId)
                        .contentType("application/json")
                        .content(vehicle))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void testDelete_NotFound() throws Exception {
        var vehicleId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        mockMvc.perform(put("/vehicles/" + vehicleId)
                        .accept("application/json"))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    private ApplicationError.ApplicationException.ApplicationExceptionCompanion prepareVehicleNotFoundExceptionCompanion(String vehicleId) {
        return VEHICLE_NOT_FOUND.exception(String.format("vehicle with id=%s doesn't exists", vehicleId)).companion;
    }


    private String prepareValidVehicle() {
        Generex generex = new Generex("[АВЕКМНОРСТУХ]\\d{3}[АВЕКМНОРСТУХ]{2}\\d{2,3}");
        return String.format("{" +
                        "\"model\": \"%s\"," +
                        "\"regPlate\": \"%s\"" +
                        "}",
                generateRandomString(),
                generex.random()
        );
    }

    private String prepareInvalidVehicle() {
        return String.format("{" +
                        "\"model\": \"%s\"," +
                        "\"regPlate\": \"Ы999ЮЯ777\"" +
                        "}",
                generateRandomString());
    }

    private String prepareNotEnoughVehicle() {
        return String.format("{" +
                        "\"model\": \"%s\"" +
                        "}",
                generateRandomString());
    }

    private String prepareBlankFieldsVehicle() {
        return "{" +
                "\"model\": \"\"," +
                "\"regPlate\": \"\"" +
                "}";
    }
}