package ru.tinkoff.parking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.tinkoff.parking.AbstractTest;
import ru.tinkoff.parking.exception.ApplicationError;
import ru.tinkoff.parking.service.SiteService;

import java.util.UUID;

import static java.lang.String.format;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.parking.exception.ApplicationError.SITE_ALREADY_EXISTS;
import static ru.tinkoff.parking.exception.ApplicationError.SITE_NOT_FOUND;

@WithMockUser(roles = "ADMIN")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class SiteControllerTest extends AbstractTest {
    private final ObjectMapper jackson = new ObjectMapper();
    @Autowired
    SiteService siteService;

    /**
     * Double add Site with same latitude and longitude
     * Expect Already exists
     *
     * @throws Exception Already exists custom Application exception
     */
    @Test
    public void testPost_AlreadyExists() throws Exception {
        var siteJson = prepareValidSite();
        mockMvc.perform(post("/sites/")
                        .contentType("application/json")
                        .content(siteJson))
                .andExpect(status().isOk())
                .andDo(print());

        mockMvc.perform(post("/sites/")
                        .contentType("application/json")
                        .content(siteJson))
                .andExpect(status().is(SITE_ALREADY_EXISTS.getCode()))
                .andDo(print());
    }


    @Test
    public void testPost_Success() throws Exception {
        var site = prepareValidSite();
        mockMvc.perform(post("/sites")
                        .contentType("application/json")
                        .content(site))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testGet_Failure() throws Exception {
        var siteId = UUID.randomUUID();
        mockMvc.perform(get("/sites").param("id", siteId.toString())
                        .accept("application/json"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGet_Success() throws Exception {
        var site = prepareValidSite();
        mockMvc.perform(post("/sites")
                .contentType("application/json")
                .content(site));

        var sites = siteService.findAll();
        var siteId = sites.get(sites.size() - 1).getId();

        mockMvc.perform(get("/sites").param("id", siteId.toString())
                        .accept("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdate_Success() throws Exception {
        var site = prepareValidSite();
        mockMvc.perform(post("/sites")
                .contentType("application/json")
                .content(site));

        var sites = siteService.findAll();
        var siteId = sites.get(sites.size() - 1).getId();

        mockMvc.perform(put("/sites/" + siteId)
                        .contentType("application/json")
                        .content(site))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testDelete_Success() throws Exception {
        var site = prepareValidSite();
        mockMvc.perform(post("/sites/")
                .contentType("application/json")
                .content(site));

        var sites = siteService.findAll();
        var siteId = sites.get(sites.size() - 1).getId();

        mockMvc.perform(delete("/sites/" + siteId + "/")
                        .accept("application/json"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testPost_FailedInvalidCoordinates() throws Exception {
        var site = prepareInvalidSite();
        mockMvc.perform(post("/sites/")
                        .contentType("application/json")
                        .content(site))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testUpdate_FailedNotEnoughFields() throws Exception {
        var site = prepareValidSite();
        var siteNotEnough = prepareNotEnoughSite();
        mockMvc.perform(post("/sites")
                .contentType("application/json")
                .content(site));

        var sites = siteService.findAll();
        var siteId = sites.get(sites.size() - 1).getId();

        mockMvc.perform(put("/sites/" + siteId)
                        .contentType("application/json")
                        .content(siteNotEnough))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testUpdate_FailedBlankFields() throws Exception {
        var site = prepareValidSite();
        var siteBlankFields = prepareBlankFieldsSite();
        mockMvc.perform(post("/sites/")
                .contentType("application/json")
                .content(site));

        var sites = siteService.findAll();
        var siteId = sites.get(sites.size() - 1).getId();

        mockMvc.perform(put("/sites/" + siteId)
                        .contentType("application/json")
                        .content(siteBlankFields))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testGet_NotFound() throws Exception {
        var siteId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        var notFoundException = jackson.writeValueAsString(prepareSiteNotFoundExceptionCompanion(siteId));
        System.out.print(notFoundException);
        mockMvc.perform(get("/sites/").param("id", siteId)
                        .accept("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(notFoundException));
    }

    @Test
    public void testUpdate_NotFound() throws Exception {
        var site = prepareValidSite();
        var siteId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        mockMvc.perform(put("/sites/" + siteId)
                        .contentType("application/json")
                        .content(site))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void testDelete_NotFound() throws Exception {
        var siteId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        mockMvc.perform(put("/sites/" + siteId)
                        .accept("application/json"))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }


    private ApplicationError.ApplicationException.ApplicationExceptionCompanion prepareSiteNotFoundExceptionCompanion(String siteId) {
        return SITE_NOT_FOUND.exception(format("site with id=%s doesn't exists", siteId)).companion;
    }


    private String prepareValidSite() {
        return format("{" +
                        "\"name\": \"%s\"," +
                        "\"latitude\": %s," +
                        "\"longitude\": %s" +
                        "}",
                generateRandomString(),
                generateRandomDecimal(-90.0, 90.0),
                generateRandomDecimal(-180.0, 180.0));
    }

    private String prepareInvalidSite() {
        return format("{" +
                        "\"name\": \"%s\"," +
                        "\"latitude\": -95.0," +
                        "\"longitude\": -360.0" +
                        "}",
                generateRandomString());
    }

    private String prepareNotEnoughSite() {
        return format("{" +
                        "\"name\": \"%s\"," +
                        "}",
                generateRandomString());
    }

    private String prepareBlankFieldsSite() {
        return "{" +
                "\"name\": \"\"," +
                "\"latitude\": \"\"," +
                "\"longitude\": \"\"" +
                "}";
    }
}