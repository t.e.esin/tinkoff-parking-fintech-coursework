package ru.tinkoff.parking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.tinkoff.parking.AbstractTest;
import ru.tinkoff.parking.exception.ApplicationError;
import ru.tinkoff.parking.service.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.parking.exception.ApplicationError.BOOKING_NOT_FOUND;

@WithMockUser(roles = "ADMIN")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class BookingControllerTest extends AbstractTest {
    private final ObjectMapper jackson = new ObjectMapper();
    @Autowired
    BookingService bookingService;

    @Autowired
    SiteService siteService;

    @Autowired
    SpotService spotService;

    @Autowired
    StaffService staffService;

    @Autowired
    VehicleService vehicleService;

    @Test
    public void testPost_Success() throws Exception {
        var booking = prepareValidBooking();
        mockMvc.perform(post("/bookings")
                        .contentType("application/json")
                        .content(booking))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testGet_Failure() throws Exception {
        var bookingId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        mockMvc.perform(get("/bookings").param("id", bookingId)
                        .accept("application/json"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGet_Success() throws Exception {
        var booking = prepareValidBooking();
        mockMvc.perform(post("/bookings")
                .contentType("application/json")
                .content(booking));

        var bookings = bookingService.findAll();
        var bookingId = bookings.get(bookings.size() - 1).getId();

        mockMvc.perform(get("/bookings").param("id", bookingId.toString())
                        .accept("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdate_Success() throws Exception {
        var booking = prepareValidBooking();
        mockMvc.perform(post("/bookings")
                .contentType("application/json")
                .content(booking));

        var bookings = bookingService.findAll();
        var bookingId = bookings.get(bookings.size() - 1).getId();

        mockMvc.perform(put("/bookings/" + bookingId)
                        .contentType("application/json")
                        .content(booking))
                .andExpect(status().isOk())
                .andDo(print());
    }

//    @Test
//    public void testDelete_Success() throws Exception {
//        var booking = prepareValidBooking();
//        mockMvc.perform(post("/bookings")
//                .contentType("application/json")
//                .content(booking));
//
//        var bookings = bookingService.findAll();
//        var bookingId = bookings.get(bookings.size() - 1).getId();
//
//        mockMvc.perform(delete("/bookings/" + bookingId)
//                        .accept("application/json"))
//                .andExpect(status().isOk())
//                .andDo(print());
//    }

    @Test
    public void testPost_FailedInvalidBlockedField() throws Exception {
        var booking = prepareInvalidBooking();
        mockMvc.perform(post("/bookings")
                        .contentType("application/json")
                        .content(booking))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testUpdate_FailedNotEnoughFields() throws Exception {
        var booking = prepareValidBooking();
        var bookingNotEnough = prepareNotEnoughBooking();
        mockMvc.perform(post("/bookings")
                .contentType("application/json")
                .content(booking));

        var bookings = bookingService.findAll();
        var bookingId = bookings.get(bookings.size() - 1).getId();

        mockMvc.perform(put("/bookings/" + bookingId)
                        .contentType("application/json")
                        .content(bookingNotEnough))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testUpdate_FailedBlankFields() throws Exception {
        var booking = prepareValidBooking();
        var bookingBlankFields = prepareBlankFieldsBooking();
        mockMvc.perform(post("/bookings/")
                .contentType("application/json")
                .content(booking));

        var bookings = bookingService.findAll();
        var bookingId = bookings.get(bookings.size() - 1).getId();

        mockMvc.perform(put("/bookings/" + bookingId)
                        .contentType("application/json")
                        .content(bookingBlankFields))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testGet_NotFound() throws Exception {
        var bookingId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        var notFoundException = jackson.writeValueAsString(prepareBookingNotFoundExceptionCompanion(bookingId));
        mockMvc.perform(get("/bookings/").param("id", bookingId)
                        .accept("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(notFoundException));
    }

    @Test
    public void testUpdate_NotFound() throws Exception {
        var booking = prepareValidBooking();
        var bookingId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        mockMvc.perform(put("/bookings/" + bookingId)
                        .contentType("application/json")
                        .content(booking))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void testDelete_NotFound() throws Exception {
        var bookingId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        mockMvc.perform(put("/bookings/" + bookingId)
                        .accept("application/json"))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    private ApplicationError.ApplicationException.ApplicationExceptionCompanion prepareBookingNotFoundExceptionCompanion(String bookingId) {
        return BOOKING_NOT_FOUND.exception(String.format("booking with id=%s doesn't exists", bookingId)).companion;
    }


    private String prepareValidBooking() {
        var staffId = staffService.findAll().get(0).getId();
        var vehicleId = vehicleService.findAll().get(0).getId();
        var spotId = spotService.findAll().get(0).getId();
        var timeStart = "2021-12-21T20:00:00.000+0500";
        return String.format("{" +
                        "\"staffId\": \"%s\"," +
                        "\"vehicleId\": \"%s\"," +
                        "\"spotId\": \"%s\"," +
                        "\"timeStart\": \"%s\"," +
                        "\"timer\": %s" +
                        "}",
                staffId,
                vehicleId,
                spotId,
                timeStart,
                1);
    }

    private String prepareInvalidBooking() {
        var staffId = staffService.findAll().get(0).getId();
        var vehicleId = vehicleService.findAll().get(0).getId();
        var spotId = spotService.findAll().get(0).getId();
        return String.format("{" +
                        "\"staffId\": \"%s\"," +
                        "\"vehicleId\": \"%s\"," +
                        "\"spotId\": \"%s\"," +
                        "\"timer\": %s" +
                        "}",
                staffId,
                vehicleId,
                spotId,
                25);
    }

    private String prepareNotEnoughBooking() {
        var staffId = staffService.findAll().get(0).getId();
        var vehicleId = vehicleService.findAll().get(0).getId();
        var spotId = spotService.findAll().get(0).getId();
        return String.format("{" +
                        "\"staffId\": \"%s\"," +
                        "\"vehicleId\": \"%s\"," +
                        "\"spotId\": \"%s\"" +
                        "}",
                staffId,
                vehicleId,
                spotId);
    }

    private String prepareBlankFieldsBooking() {
        return  "{" +
                "\"staffId\": \"\"," +
                "\"vehicleId\": \"\"," +
                "\"spotId\": \"\"" +
                "\"timer\": \"\"" +
                "}";
    }
}