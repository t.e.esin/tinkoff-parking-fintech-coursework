package ru.tinkoff.parking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.tinkoff.parking.AbstractTest;
import ru.tinkoff.parking.exception.ApplicationError;
import ru.tinkoff.parking.service.StaffService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.parking.exception.ApplicationError.EMPLOYEE_NOT_FOUND;

@WithMockUser(roles = "ADMIN")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class StaffControllerTest extends AbstractTest {
    private final ObjectMapper jackson = new ObjectMapper();
    @Autowired
    StaffService staffService;

    @Test
    public void testPost_Success() throws Exception {
        var employee = prepareValidEmployee();
        mockMvc.perform(post("/staff")
                        .contentType("application/json")
                        .content(employee))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testGet_Failure() throws Exception {
        var employeeId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        mockMvc.perform(get("/staff").param("id", employeeId)
                        .accept("application/json"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGet_Success() throws Exception {
        var employee = prepareValidEmployee();
        mockMvc.perform(post("/staff")
                .contentType("application/json")
                .content(employee));

        var staff = staffService.findAll();
        var employeeId = staff.get(staff.size() - 1).getId();

        mockMvc.perform(get("/staff").param("id", employeeId.toString())
                        .accept("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdate_Success() throws Exception {
        var employee = prepareValidEmployee();
        mockMvc.perform(post("/staff")
                .contentType("application/json")
                .content(employee));

        var staff = staffService.findAll();
        var employeeId = staff.get(staff.size() - 1).getId();

        mockMvc.perform(put("/staff/" + employeeId)
                        .contentType("application/json")
                        .content(employee))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testDelete_Success() throws Exception {
        var employee = prepareValidEmployee();
        mockMvc.perform(post("/staff/")
                .contentType("application/json")
                .content(employee));

        var staff = staffService.findAll();
        var employeeId = staff.get(staff.size() - 1).getId();

        mockMvc.perform(delete("/staff/" + employeeId)
                        .accept("application/json"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testPost_FailedInvalidBlockedField() throws Exception {
        var employee = prepareInvalidEmployee();
        mockMvc.perform(post("/staff")
                        .contentType("application/json")
                        .content(employee))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testUpdate_FailedNotEnoughFields() throws Exception {
        var employee = prepareValidEmployee();
        var employeeNotEnough = prepareNotEnoughEmployee();
        mockMvc.perform(post("/staff")
                .contentType("application/json")
                .content(employee));

        var staff = staffService.findAll();
        var employeeId = staff.get(staff.size() - 1).getId();

        mockMvc.perform(put("/staff/" + employeeId)
                        .contentType("application/json")
                        .content(employeeNotEnough))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testUpdate_FailedBlankFields() throws Exception {
        var employee = prepareValidEmployee();
        var employeeBlankFields = prepareBlankFieldsEmployee();
        mockMvc.perform(post("/staff/")
                .contentType("application/json")
                .content(employee));

        var staff = staffService.findAll();
        var employeeId = staff.get(staff.size() - 1).getId();

        mockMvc.perform(put("/staff/" + employeeId)
                        .contentType("application/json")
                        .content(employeeBlankFields))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testGet_NotFound() throws Exception {
        var employeeId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        var notFoundException = jackson.writeValueAsString(prepareEmployeeNotFoundExceptionCompanion(employeeId));
        mockMvc.perform(get("/staff/").param("id", employeeId)
                        .accept("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(notFoundException));
    }

    @Test
    public void testUpdate_NotFound() throws Exception {
        var employee = prepareValidEmployee();
        var employeeId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        mockMvc.perform(put("/staff/" + employeeId)
                        .contentType("application/json")
                        .content(employee))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void testDelete_NotFound() throws Exception {
        var employeeId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        mockMvc.perform(put("/staff/" + employeeId)
                        .accept("application/json"))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    private ApplicationError.ApplicationException.ApplicationExceptionCompanion prepareEmployeeNotFoundExceptionCompanion(String employeeId) {
        return EMPLOYEE_NOT_FOUND.exception(String.format("employee with id=%s doesn't exists", employeeId)).companion;
    }


    private String prepareValidEmployee() {
        return String.format("{" +
                        "\"name\": \"%s\"" +
                        "}",
                generateRandomString());
    }

    private String prepareInvalidEmployee() {
        return String.format("{" +
                        "\"name\": \"%s\"," +
                        "\"blocked\": blocked" +
                        "}",
                generateRandomString());
    }

    private String prepareNotEnoughEmployee() {
        return "{}";
    }

    private String prepareBlankFieldsEmployee() {
        return "{" +
                "\"name\": \"\"," +
                "}";
    }
}