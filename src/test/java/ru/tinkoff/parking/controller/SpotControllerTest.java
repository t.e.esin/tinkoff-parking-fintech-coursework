package ru.tinkoff.parking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.tinkoff.parking.AbstractTest;
import ru.tinkoff.parking.exception.ApplicationError;
import ru.tinkoff.parking.service.SiteService;
import ru.tinkoff.parking.service.SpotService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.parking.exception.ApplicationError.SPOT_NOT_FOUND;

@WithMockUser(roles = "ADMIN")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class SpotControllerTest extends AbstractTest {
    private final ObjectMapper jackson = new ObjectMapper();
    @Autowired
    SpotService spotService;

    @Autowired
    SiteService siteService;

    @Test
    public void testPost_Success() throws Exception {
        var spot = prepareValidSpot();
        mockMvc.perform(post("/spots")
                        .contentType("application/json")
                        .content(spot))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testGet_Failure() throws Exception {
        var spotId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        mockMvc.perform(get("/spots").param("id", spotId)
                        .accept("application/json"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGet_Success() throws Exception {
        var spot = prepareValidSpot();
        mockMvc.perform(post("/spots")
                .contentType("application/json")
                .content(spot));

        var spots = spotService.findAll();
        var spotId = spots.get(spots.size() - 1).getId();

        mockMvc.perform(get("/spots").param("id", spotId.toString())
                        .accept("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdate_Success() throws Exception {
        var spot = prepareValidSpot();
        mockMvc.perform(post("/spots")
                .contentType("application/json")
                .content(spot));

        var spots = spotService.findAll();
        var spotId = spots.get(spots.size() - 1).getId();

        mockMvc.perform(put("/spots/" + spotId)
                        .contentType("application/json")
                        .content(spot))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testDelete_Success() throws Exception {
        var spot = prepareValidSpot();
        mockMvc.perform(post("/spots")
                .contentType("application/json")
                .content(spot));

        var spots = spotService.findAll();
        var spotId = spots.get(spots.size() - 1).getId();

        mockMvc.perform(delete("/spots/" + spotId)
                        .accept("application/json"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testPost_FailedInvalidBlockedField() throws Exception {
        var spot = prepareInvalidSpot();
        mockMvc.perform(post("/spots")
                        .contentType("application/json")
                        .content(spot))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testUpdate_FailedNotEnoughFields() throws Exception {
        var spot = prepareValidSpot();
        var spotNotEnough = prepareNotEnoughSpot();
        mockMvc.perform(post("/spots")
                .contentType("application/json")
                .content(spot));

        var spots = spotService.findAll();
        var spotId = spots.get(spots.size() - 1).getId();

        mockMvc.perform(put("/spots/" + spotId)
                        .contentType("application/json")
                        .content(spotNotEnough))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testUpdate_FailedBlankFields() throws Exception {
        var spot = prepareValidSpot();
        var spotBlankFields = prepareBlankFieldsSpot();
        mockMvc.perform(post("/spots/")
                .contentType("application/json")
                .content(spot));

        var spots = spotService.findAll();
        var spotId = spots.get(spots.size() - 1).getId();

        mockMvc.perform(put("/spots/" + spotId)
                        .contentType("application/json")
                        .content(spotBlankFields))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testGet_NotFound() throws Exception {
        var spotId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        var notFoundException = jackson.writeValueAsString(prepareSpotNotFoundExceptionCompanion(spotId));
        mockMvc.perform(get("/spots/").param("id", spotId)
                        .accept("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(notFoundException));
    }

    @Test
    public void testUpdate_NotFound() throws Exception {
        var spot = prepareValidSpot();
        var spotId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        mockMvc.perform(put("/spots/" + spotId)
                        .contentType("application/json")
                        .content(spot))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void testDelete_NotFound() throws Exception {
        var spotId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        mockMvc.perform(put("/spots/" + spotId)
                        .accept("application/json"))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    private ApplicationError.ApplicationException.ApplicationExceptionCompanion prepareSpotNotFoundExceptionCompanion(String spotId) {
        return SPOT_NOT_FOUND.exception(String.format("spot with id=%s doesn't exists", spotId)).companion;
    }


    private String prepareValidSpot() {
        var parkingSite = siteService.findAll().get(0).getId();
        return String.format("{" +
                        "\"name\": \"%s\"," +
                        "\"location\": \"%s\"," +
                        "\"parkingSite\": \"%s\"" +
                        "}",
                generateRandomString(),
                generateRandomString(),
                parkingSite);
    }

    private String prepareInvalidSpot() {
        var ps = "zzzzzzzz-zzzz-zzzz-zzzz-zzzzzzzzzzzz";
        return String.format("{" +
                        "\"name\": \"%s\"," +
                        "\"location\": \"%s\"," +
                        "\"parkingSite\": \"%s\"" +
                        "}",
                generateRandomString(),
                generateRandomString(),
                ps);
    }

    private String prepareNotEnoughSpot() {
        return String.format("{" +
                "\"name\": \"%s\"" +
                "}",
                generateRandomString());
    }

    private String prepareBlankFieldsSpot() {
        return "{" +
                "\"name\": \"\"," +
                "\"location\": \"\"," +
                "\"parkingSite\": \"\"" +
                "}";
    }
}